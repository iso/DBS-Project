import au.com.bytecode.opencsv.CSVReader;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class HasRelation {

    public static void main(String[] args) {
        HasRelation hasRelation = new HasRelation();
        String[] topLayerTuples = hasRelation.makeTuples();
        hasRelation.write(topLayerTuples);
    }

    /**
     * nutzt das erstellte Array um einen neuen csv-file zu erstellen
     */
    private void write(String[] topLayerTuples) {
        try {
            /**
             * initialisiere Writer
             */
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tweetHashtagsHas.csv"), "Cp1252"));
            System.out.println(topLayerTuples.length);
            writer.write("id; text" + System.getProperty("line.separator"));
            /**
             * iteriere über die Elemente des Arrays
             * jedes Element ist eine Zeile
             */
            for(String line : topLayerTuples) {
                System.out.println(line);
                writer.write(line + System.getProperty("line.separator") );
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Auslagerung der Initalisierungen etc. aus der main-Methode
     */

    private String[] makeTuples() {
        CSVReader reader;
        CSVReader reader2;
        /**
         * aus der Betrachtung das wir nihct wissen aus wievielen Einträgen unsere neue Liste besteht initialisieren wir sie mit 0 und berechnen innerhalb der Funktion ihre wahre Größe
         */
        String[] ausgabe = new String[0];
        try {
            /**
             * initiiere reader
             * in unserem Fall nutzen wir einen CSVReader
             * der frei verfügbaren opencsv-library,
             * was das Splitten der einzelnen Zeilen verinfacht,
             * da CSVReader auch Angaben wie ";;" korrekt liest
             */
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("cleanedElection.csv"), "Cp1252"));
            reader = new CSVReader(bufferedReader, ';', '\"', '\\');

            /**
             * überspringen der Bezeichner-Zeile
             */
            reader.readNext();

            // Wir nutzen eine Hilfsfunktion um herauszufinden wie groß das Array sein muss das wir brauchen um die has-Relation zu speichern
            int size = countHashtags(reader);
            ausgabe = new String[size];

            // Anschließend erstellen wir dieses Array und

            // Nutzen eine weitere Funktion um herauszufinden welche Hashtags in welchen Tweets enthalten waren und das Array mit diesen Informationen geben wir aus.
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("cleanedElection.csv"), "Cp1252"));
            reader2 = new CSVReader(bufferedReader2, ';', '\"', '\\');


            /**
             * überspringen der Bezeichner-Zeile
             */
            reader2.readNext();

            makeHas(reader2,ausgabe);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ausgabe;
    }


    /**
     * Iteriert über jede einzelne Zeile des CSV-files und
     * nimmt nunächst alle Hashtags aus dem Tweet. Anschließend erzeug es pro Hashtag einen neuen Eintrag in unsere ausgabe String[]. Der Inhalt ist  die Id des Tweets und der komplette Hashtag.
     */
    private String[] makeHas(CSVReader reader,String[] ausgabe) throws IOException {
        String[] tweet;
        HasRelation hasRelation = new HasRelation();
        int counter = 0;
        ArrayList<String> ausgabeliste = new ArrayList<String>();
        while ((tweet = reader.readNext()) != null) {

            try {
                String[] hashtags = hasRelation.tweetToHashtag(tweet[2]);

                int min = counter;
                for (int i = min; i < (min + hashtags.length); i++) {

                    ausgabe[i] = tweet[0] + ";" + hashtags[i - min];
                    counter++;

                }
            }
            catch(ArrayIndexOutOfBoundsException ignored){}

        }
        return ausgabe;
    }



    // Die Funktion zählt alle Hashtags innerhalb der CSV datei die mithilfe des CSVReaders ausgelesen werden kann.
    // DIes benötigen wir um ein Array mit passender Größe zu initialisieren
    int countHashtags(CSVReader reader)throws IOException
    {
        String[] tweet;
        int sumHashtags = 0;
        HasRelation hasRelation = new HasRelation();
        while ((tweet = reader.readNext()) != null){
            String[] hashtags = new String[0];
            try {
                hashtags = hasRelation.tweetToHashtag(tweet[2]);
            }
            catch(ArrayIndexOutOfBoundsException e){

            }
            for(int i=0;i<hashtags.length;i++)
            {
                sumHashtags++;
            }
        }
        return sumHashtags;
    }


    // Eine Funktion wird benutzt um aus einem tweet (dem Eingabeparameter) ein Array von Hashtags zu machen, also alle Hashtags des Tweet herauszubekommen.
    String[] tweetToHashtag(String text){
        // Zu Beginn werden zwei Wahrheitswerte initialisiert um die Sonderfälle zu berücksichtigen
        boolean readedChar = false;
        boolean inHashtag = false;

        // und zwei Array Listen  um die Hashtags zusammenzufügen und in eine Ausgabeliste zu speichern
        ArrayList<Character> hashtaglist = new ArrayList<Character>();
        ArrayList<String> ausgabeliste = new ArrayList<String>();

        // Mithilfe dieser Schleife gehen wir durch jedes Zeichen im Tweet
        // Die Wahrheitswerte helfen uns zu überrpüfen wann wir nach lesen eines # und den daruaffolgenenden Zeichen ein neuen Hashtag zu einem eintrag in unsere Liste umwandeln können,
        // Dafür gibt es nähmlich folgende Regeln:
        // Vor einem Hashtag kommt entweder beliebig viele Sonderzeichen oder Leerzeichen und wenn nicht dann ist es der tweetanfang
        // In einem Hashtag kommt nach dem '#' mindestens 1 nicht Sonderzeichen
        // Wenn ein Sonderzeichen kommt oder das tweetende dann ist der Hashtag beendet
        // Sonderfall 1: In einem potenziellen Hashtag nach einem '#' kommt ohne Sonderzeichen ein weiteres # dann ist es kein Hashtag mehr
        // Sonderfall 2: In einem potenziellen Hashtag direkt nach einem '#' kommt eine Zahl, dann ist es auch kein Hashtag
        // Sonderfall 3: Nach einem '#' kommt Sonderzeichen oder '#' dann auch kein Hashtag

        String sonderzeichen = " .!$%^&*+";
        String zahlen = "0123456789";

        char[] tweet = text.toCharArray();
        for(int i = 0; i< tweet.length;i++)
        {
            // Der Wahrheitswert um zu überprüfen ob wir mit einem Hashtag beginnen können.
            // Zu Beginn False
            if(readedChar){
                if(tweet[i]==' '){
                    readedChar = false;
                }

            }else{
                // Wahrheitswert um zu überprüfen ob wir in einem potenziellen Hashtag sind
                if(inHashtag){
                    // Sonderfall 2 und 3 wird überprüft
                    if(hashtaglist.size() == 1){
                        if(stringContainsChar(zahlen,tweet[i]) || stringContainsChar(sonderzeichen,tweet[i]) || tweet[i] == '#')
                        {
                            inHashtag = false;
                            hashtaglist.clear();
                        }
                        else // Neues Zeichen für Hashtag
                        {
                            hashtaglist.add(tweet[i]);
                        }
                        // unser potenzielles Hashtag besteht aus minestens 1 gültigen Zeichen
                    }else{
                        if(tweet[i]=='#' ){

                            inHashtag = false;
                            hashtaglist.clear();

                        }
                        else if(stringContainsChar(sonderzeichen,tweet[i])) // Hashtag wird in Ausgabeliste hinzugefügt
                        {
                            inHashtag = false;
                            String hashtag = "";
                            for(int j = 0; j < hashtaglist.size();j++){
                                hashtag = hashtag + hashtaglist.get(j);
                            }

                            ausgabeliste.add(hashtag);
                            hashtaglist.clear();
                        }
                        else // Neues Zeichen für Hashtag
                        {
                            hashtaglist.add(tweet[i]);
                        }
                    }
                }else{
                    //inHashtag == false
                    //wir sind also nicht im Hashtag und machen eine Fallunterscheidung um die Wahrheitswerte zu aktualisieren
                    if (tweet[i] == '#'){
                        inHashtag = true;
                        hashtaglist.add(tweet[i]);
                    }else if(stringContainsChar(sonderzeichen,tweet[i]))
                    {

                    }else{
                        // wenn kein Sonderzeichen gelesen wird
                        readedChar = true;
                    }



                }
            }
        }
        if(hashtaglist.size()>1) // wenn die Liste nicht leer ist noch letzes Hashtag hinzufügen falls nötig;
        {
            String hashtag = "";
            for(int j = 0; j < hashtaglist.size();j++){
                hashtag = hashtag + hashtaglist.get(j);


            }
            ausgabeliste.add(hashtag);
        }
        String[] ausgabe = new String[ausgabeliste.size()];

        ausgabe = ausgabeliste.toArray(new String[0]);
        return ausgabe;
    }


    // Die Hilfsfunkton erlaubt uns zu überprüfen ob ein Char in einem String auftritt
    public boolean stringContainsChar(String text,char chr)
    {
        for (int i = 0;i < text.length();i++)
        {
            if (text.charAt(i) == chr)
            {
                return true;
            }

        }
        return false;

    }








}
